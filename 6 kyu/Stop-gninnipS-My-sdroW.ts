// link to kata https://www.codewars.com/kata/5264d2b162488dc400000001
export function spinWords(words: string): string {
    return words.split(' ').map((item) => item.length >= 5 ? item.split('').reverse().join(''): item).join(' ')
}