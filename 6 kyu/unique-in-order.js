// link to kata https://www.codewars.com/kata/54e6533c92449cc251001667
const useRegular = (value, regexp, uniqItem) => {
    return value.replace(regexp, uniqItem)
}

const uniqueInOrder = (iterable) => {
    let value = typeof iterable === 'string' ? iterable.toString() : iterable.join('')
    const uniq = [...new Set(value)]
    for (let i = 0; i <= uniq.length - 1; i++) {
        value = useRegular(value, new RegExp(`${uniq[i]}+`, 'g'), uniq[i]);
    }
    return typeof iterable[0] === 'number' ? [...value].map(x => parseInt(x)) : [...value]
}