// link to kata https://www.codewars.com/kata/525f50e3b73515a6db000b83
export function createPhoneNumber(numbers: number[]): string {
    const text = numbers.join("")
    return `(${text.slice(0,3)}) ${text.slice(3, 6)}-${text.slice(6, numbers.length)}`
}