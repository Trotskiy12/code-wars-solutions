// link to kata https://www.codewars.com/kata/54b42f9314d9229fd6000d9c
export function persistence(num: number): number {
    let count = 0;
    while (num > 9) {
        num = Array.from(num.toString(), Number).reduce((a, b) => a * b)
        count++;
    }
    return count
}