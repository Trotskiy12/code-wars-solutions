// link to kata https://www.codewars.com/kata/526571aae218b8ee490006f4
export function countBits(n: number): number {
    return n > 0 ? (n.toString(2).match(/1/g) || []).length : 0;
}