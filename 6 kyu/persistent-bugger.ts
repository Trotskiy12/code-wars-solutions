// link to kata https://www.codewars.com/kata/55bf01e5a717a0d57e0000ec
export function persistence(num: number): number {
    let count = 0;
    while (num > 9) {
        num = Array.from(num.toString(), Number).reduce((a, b) => a * b)
        count++;
    }
    return count
}