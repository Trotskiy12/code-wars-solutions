// link to kata https://www.codewars.com/kata/541c8630095125aba6000c00
export const numberToArray = (value: number): number[] => {
    return Array.from(String(value), Number);
}

export const digitalRoot = (n:number):number => {
  if(n < 10) {
        return n 
    } else {
        const arr = numberToArray(n)
        n = arr.reduce((a, b) => a + b, 0)
        return digitalRoot(n)
    }
};