// link to kata https://www.codewars.com/kata/5526fc09a1bbd946250002dc
export function findOutlier(integers: number[]): number {
    const odd = integers.filter(item => item % 2 !== 0)
    return odd.length === 1 ? Number(odd) : Number(integers.filter(item => item % 2 === 0))
}