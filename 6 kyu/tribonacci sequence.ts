// link to kata https://www.codewars.com/kata/556deca17c58da83c00002db
export function tribonacci([a, b, c]: [number, number, number], n: number): number[] {
    let res: Array<number> = [];
    if (n >= 3) {
        n -= 3;
        res.push(a,b,c)
        while (n > 0) {
          res.push(res.slice(-3).reduce((a,b) => a + b))
          n--;
        }
        return res
    } 
    return [a,b,c].slice(0, n)
  }