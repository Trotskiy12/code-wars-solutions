// link to kata https://www.codewars.com/kata/54da5a58ea159efa38000836
function findOdd(arr) {
    return arr[arr.map(item => arr.filter((el) => el === item).length % 2 != 0).indexOf(true)]
}