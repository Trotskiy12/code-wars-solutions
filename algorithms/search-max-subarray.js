// Найти подпоследовательность с максимальной суммой
// В основе Алгоритм Кадане на поиск максимальной суммы подпоследовательности
const maxSequence = function (arr) {
    if (arr.length === 0) return 0;
    let maxTillNow = arr[0];
    let maxEnding = 0;
    let startSubArray = 0;
    let endSubArray = 0;
    let minPos = -1;
    for (let i = 0; i < arr.length; i++) {
        maxEnding = maxEnding + arr[i];
        if (maxEnding < 0) {
            maxEnding = 0
            minPos = i;
        }
        if (maxTillNow < maxEnding) {
            maxTillNow = maxEnding;
            startSubArray = minPos + 1
            endSubArray = i;
        }
    }
    console.log('Максимальная сумма подпоследовательности', maxTillNow)
    return arr.slice(startSubArray, endSubArray + 1)
}

console.log(maxSequence([-2, 1, -3, 4, -1, 2, 1, -5, 4]))

// [-2, 1, -3, 4, -1, 2, 1, -5, 4] -> 1
// 6
