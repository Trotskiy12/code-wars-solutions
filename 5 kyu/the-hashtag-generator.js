function generateHashtag(str) {
    const capitalizeWords = str.split(' ').map(item => item.replace(item.charAt(0), item.charAt(0).toUpperCase())).join(' ')
    const wordsWitoutSpaces = capitalizeWords.replace(/ /g, "")
    if (wordsWitoutSpaces.length >= 140 || wordsWitoutSpaces.length === 0) return false;
    return "#" + wordsWitoutSpaces;
}

console.log(generateHashtag('Looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong Cat'))
console.log(generateHashtag(""))
console.log(generateHashtag(" ".repeat(200)))
console.log(generateHashtag("Do We have A Hashtag"))
console.log(generateHashtag("code" + " ".repeat(140) + "wars"))