// link to kata https://www.codewars.com/kata/5279f6fe5ab7f447890006a7
function pickPeaks(arr) {
    return arr.reduce((res, el, i, self) => {
        if (el > arr[i - 1] && el > arr[i + 1] || el > self[i - 1] && el === self[i + 1] && self.slice(i).find(item => item !== el) < el) {
            res.pos.push(i)
            res.peaks.push(el)
        }
        return res;
    }, { pos: [], peaks: [] })
}