function firstNonRepeatingLetter(s) {
    return [...s].find(e => s.match(new RegExp(e, 'gi')).length === 1) ?? ''
}

console.log(firstNonRepeatingLetter('sTrreess')) // t