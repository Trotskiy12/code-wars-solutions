function fibonacci(n) {
    if (n <= 1) return 1;
    return fibonacci(n - 1) + fibonacci(n - 2);
}
function productFib(prod) {
    let n = 0;
    while (fibonacci(n) * fibonacci(n + 1) !== prod) {
        if (fibonacci(n) * fibonacci(n + 1) > prod) {
            return [fibonacci(n), fibonacci(n + 1), false];
        }
        n++;
    }
    return [fibonacci(n), fibonacci(n + 1), true]
}

/* best from Code wars
Анализ: Данное решение в разы удачнее, так как не используется функция фибоначчи
Выслеженна логика, что два числа достаточно увеличить по логике:
n каждый раз увеличивается на nNext - n, а nNext n + Next по сути мы и получаем те самые фиббоначи
+ ко всему очевидно, 
что данная реализация отрабатывает быстрее, так как не тратиться время на вызовы рекурсивной функции
1 1
1 2
2 3
3 5
8 13
13 21
21 34
34 55
55 89
89 144
*/
function productFib2(prod) {
    var n = 0;
    var nNext = 1;
    while (n * nNext < prod) {
        nNext = n + nNext;
        n = nNext - n;
    }
    return [n, nNext, n * nNext === prod];
}