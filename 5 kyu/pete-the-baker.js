function cakes(recipe, available) {
    const recipeKeys = Object.keys(recipe)
    let minCakes = Number.MAX_SAFE_INTEGER;
    for (let i = 0; i < recipeKeys.length; i++) {
        if (available.hasOwnProperty(recipeKeys[i])) {
            tmp = Math.floor(available[recipeKeys[i]] / recipe[recipeKeys[i]])
            if (tmp < minCakes)
                minCakes = tmp;
        } else {
            minCakes = 0;
        }
    }
    return minCakes
}

const result = cakes({ flour: 500, sugar: 200, eggs: 1 }, { flour: 1200, sugar: 1200, eggs: 5, milk: 200 })
console.log(result)

// best on CodeWars
// Анализ
// Проходим по ключам рецепта
// Выбираем минимальное из округленного деления значения по ключу из available
// если нет в available - вернуть 0
// Дальше вернем минимальное из результата деления и val
function cakes(recipe, available) {
    return Object.keys(recipe).reduce(function (val, ingredient) {
        return Math.min(Math.floor(available[ingredient] / recipe[ingredient] || 0), val)
    }, Infinity)
}