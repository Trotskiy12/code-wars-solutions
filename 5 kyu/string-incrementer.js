function incrementString(strng) {
    const digitsFromStr = strng.match(/\d+$/) ? strng.match(/\d+$/)[0] : '0';
    const strWihtoutChars = strng.replace(new RegExp(digitsFromStr + '$', 'g'), '');
    const delta = (digitsFromStr.length - (parseInt(digitsFromStr) + 1).toString().length)
    return `${strWihtoutChars}${"0".repeat(delta < 0 ? 0 : delta)}${parseInt(digitsFromStr) + 1}`
}

console.log(incrementString("foobar000"));
console.log(incrementString("foobar999"));
console.log(incrementString("foobar00999"));
console.log(incrementString("foo"));
console.log(incrementString("foobar001"));
console.log(incrementString("foobar1"));
console.log(incrementString("1"));
console.log(incrementString("009"));
console.log(incrementString("fo99obar99"));