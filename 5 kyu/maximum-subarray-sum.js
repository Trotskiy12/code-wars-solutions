var maxSequence = function (arr) {
    // если пустой массив, то вернут ноль
    if (arr.length === 0) return 0;
    // за максимальную сумму берем первый элемент массива
    let maxSum = arr[0];
    // за минимальную ноль (хотя лучше брать максимально маленькое число из возможных)
    let minSum = Number.MIN_SAFE_INTEGER;
    // обходим массив
    for (let i = 0; i < arr.length; i++) {
        // в минимальную сумму записываем суммы всех элементов
        minSum += arr[i];
        // если эта сумма меньше нуля, то вернём ноль
        if (minSum < 0) minSum = 0;
        // если минимальная сумма больше максимальной
        if (maxSum < minSum) {
            // переприсвоим максимальную сумму
            maxSum = minSum;
        }
    }
    return maxSum;
}

console.log(maxSequence([-2, 1, -3, 4, -1, 2, 1, -5, 4]))