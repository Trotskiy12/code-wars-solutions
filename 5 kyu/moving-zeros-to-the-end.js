// link to kata https://www.codewars.com/kata/52597aa56021e91c93000cb0
function moveZeros(arr) {
    let zeroElementCount = arr.filter(item => item === 0).length;
    arr = arr.filter(item => item !== 0)
    while (zeroElementCount > 0) {
        zeroElementCount--;
        arr.push(0);
    }
    return arr
}