// link to kata https://www.codewars.com/kata/515bb423de843ea99400000a
class PaginationHelper {
    constructor(collection, itemsPerPage) {
        this.collection = collection;
        this.itemsPerPage = itemsPerPage;
    }
    itemCount() {
        return this.collection.length
    }
    pageCount() {
        return Math.ceil(this.collection.length / this.itemsPerPage);
    }

    getElementsOnPages() {
        let itemCount = this.itemCount();
        let res = [];
        while (itemCount >= this.itemsPerPage) {
            res.push(this.itemsPerPage)
            itemCount -= this.itemsPerPage;
        }
        res.push(itemCount)
        return res;
    }

    pageItemCount(pageIndex) {
        if (this.itemCount() === 0 || pageIndex < 0 || pageIndex >= this.pageCount()) return -1;
        const pages = this.getElementsOnPages()
        return pageIndex < pages.length ? pages[pageIndex] : -1;
    }
    pageIndex(itemIndex) {
        if (this.itemCount() === 0 || itemIndex < 0) return -1;
        if (itemIndex < 0 || itemIndex >= this.itemCount()) return -1;
        return Math.trunc(itemIndex / this.itemsPerPage)
    }
}