// link to kata https://www.codewars.com/kata/520b9d2ad5c005041100000f
function pigIt(str) {
    let res = str.replace(/[a-zA-Z]+/g, (match) => (match.slice(1, match.length) + match.slice(0, 1)) + 'ay')
    return res
}