// link to kata: https://www.codewars.com/kata/513e08acc600c94f01000001
export function numberToBase16(color: number): string {
    if(color < 0) return '00'
    else if(color > 255) return 'FF'
    else if(color >= 16) return color.toString(16)
    return 0 + color.toString(16)
 }
 export function rgb(r: number, g: number, b: number): string {
       return `${numberToBase16(r)}${numberToBase16(g)}${numberToBase16(b)}`.toUpperCase()
 }